#include <math.h>
#include "euclidean_heuristic.h"
#include "xy_graph.h"

#define Pi 0.000000017453292519943295 //#Pi/180/1000,000

warthog::euclidean_heuristic::euclidean_heuristic(warthog::graph::xy_graph* g) 
{ 
    g_ = g;
    hscale_ = 1;
}

warthog::euclidean_heuristic::~euclidean_heuristic() 
{ }

double
warthog::euclidean_heuristic::h(warthog::sn_id_t id, warthog::sn_id_t id2)
{
    int32_t x, x2;
    int32_t y, y2;
    g_->get_xy((uint32_t)id, x, y);
    g_->get_xy((uint32_t)id2, x2, y2);
    return this->h(x, y, x2, y2);
}

double
warthog::euclidean_heuristic::h(int32_t x, int32_t y, int32_t x2, int32_t y2)
{
    // NB: precision loss when warthog::cost_t is an integer
    double dx = x-x2;
    double dy = y-y2;
    return sqrt(dx*dx + dy*dy) * hscale_;
}

double
warthog::euclidean_heuristic::h_geo(warthog::sn_id_t id, warthog::sn_id_t id2)
{
    int32_t x, x2;
    int32_t y, y2;
    g_->get_xy((uint32_t)id, x, y);
    g_->get_xy((uint32_t)id2, x2, y2);
    return floor(this->h_geo(x, y, x2, y2));
}

double
warthog::euclidean_heuristic::h_geo(int32_t lon1, int32_t lat1, int32_t lon2, int32_t lat2)
{
    // NB: precision loss when warthog::cost_t is an integer
    
    double a = 0.5 - cos((lat2 - lat1) * Pi)/2 + cos(lat1 * Pi) * cos(lat2 * Pi) * (1 - cos((lon2 - lon1) * Pi)) / 2;
    return hscale_*127420000* asin(sqrt(a)) ; //#2*R*asin... # converted to m from km then converted to 0.1m;
}

void
warthog::euclidean_heuristic::set_hscale(double hscale)
{
    if(hscale > 0)
    {
        hscale_ = hscale;
    }
}

double
warthog::euclidean_heuristic::get_hscale() 
{ 
    return hscale_; 
}

size_t
warthog::euclidean_heuristic::mem() 
{ 
    return sizeof(this); 
}

