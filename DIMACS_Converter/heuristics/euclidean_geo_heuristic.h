#ifndef WARTHOG_EUCLIDEAN_GEO_HEURISTIC_H
#define WARTHOG_EUCLIDEAN_GEO_HEURISTIC_H

// euclidean_geo_heuristic.h
//
// Heuristic for measuring geometric distances on earth.
//
// @author: 
// @created: 
//
//

#include "constants.h"
#include "forward.h"

namespace warthog
{

typedef void (*xyFn)(uint32_t id, int32_t& x, int32_t& y);
class euclidean_geo_heuristic
{
    public:
        euclidean_geo_heuristic(warthog::graph::xy_graph* g);
        ~euclidean_geo_heuristic();

        double
        h(sn_id_t id, sn_id_t id2);

		double
		h(int32_t x, int32_t y, int32_t x2, int32_t y2);

        void
        set_hscale(double hscale);

        double
        get_hscale();

        size_t
        mem(); 

	private:
        warthog::graph::xy_graph* g_;
        double hscale_;

};

}

#endif

