# READ ME #
DIMACS graph file converter to xy graph

graphs can be downloaded from:
http://users.diag.uniroma1.it/challenge9/download.shtml

#### To compile
```bash
        make
```
#### To run
Convert DIMACS files into a sigle graph.xy file. For example:

```bash
./bin/alldimacs2xy --input ./BAY-road-d.USA.co ./BAY-road-d.USA.gr ./BAY-road-t.USA.gr > ./BAY.xy
```