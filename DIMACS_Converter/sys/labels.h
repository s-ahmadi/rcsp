#ifndef WARTHOG_LABELS_H
#define WARTHOG_LABELS_H

// labels.h
//
// @author: 
// @created: 
//

#include "constants.h"

namespace warthog
{
    // shortest path labels
    struct sp_label
	{
    	warthog::cost_uint f_, r_, g_= warthog::COST_UINT_MAX;
		uint32_t priority_;
		bool status_ = false;
		warthog::sn_id_t parent_id_;
		
		sp_label(warthog::cost_uint f, warthog::cost_uint g, warthog::cost_uint r, warthog::sn_id_t parent_id)
		{
			f_ = f; g_ = g; r_ = r;
			parent_id_ = parent_id;
		}
		sp_label(warthog::cost_uint f,warthog::cost_uint g,warthog::cost_uint r)
		{
			f_ = f; g_ = g; r_ = r;
		}
		sp_label()
		{}

	};

    // constrained shortest path label
    struct csp_label
	{
		warthog::cost_uint f_;
    	warthog::cost_uint g_ , r_;
		uint32_t priority_;
		// bool status_ = true;
		// bool join_ = false;
		warthog::sn_id_t id_;
		// csp_label* parent_ = 0;
        csp_label* next_ = 0; // linked for expanded list
		// csp_label* next_b_ = 0;
		
		// csp_label* next_becket_ = 0; // linked for bucket list
		
		// csp_label(warthog::cost_uint f, warthog::cost_uint g, warthog::cost_uint r, warthog::sn_id_t id, csp_label* parent)
		// {
		// 	f_ = f; g_ = g; r_ = r;
		// 	id_ = id;
		// 	parent_ = parent;
		// }

		csp_label(warthog::cost_uint g, warthog::cost_uint r, warthog::sn_id_t id)
		{
			g_ = g; r_ = r; id_ = id;			
		}
		
		csp_label(warthog::cost_uint f,warthog::cost_uint g,warthog::cost_uint r, warthog::sn_id_t id)
		{
			f_ = f; g_ = g; r_ = r; id_ = id;
		}
		csp_label()
		{g_ = 0; r_ = 0; id_ = 0; next_ = 0;}

	};

	struct master_label
	{
    	// warthog::cost_uint non_dom_pulse_g_[3] = { warthog::COST_UINT_MAX, warthog::COST_UINT_MAX, warthog::COST_UINT_MAX };
		// warthog::cost_uint non_dom_pulse_r_[3] = { warthog::COST_UINT_MAX, warthog::COST_UINT_MAX, warthog::COST_UINT_MAX };
		struct sp_label shortest_path_[2];
		warthog::cost_uint r_min_= warthog::COST_UINT_MAX;
		csp_label* head_ = NULL;
		csp_label* tail_ = NULL;
		// bool active_pulse_;		
	};
    // struct master_label_hash
	// {
	// 	std::size_t operator () ( master_label* const& l ) const
    // 	{
    //     	return l->g_;
    // 	}
	// };
    // typedef std::unordered_set< master_label*, master_label_hash > label2set;
	
    struct min_q 
    { static const bool is_min_ = true; };


}

#endif


