#include "csp_label_pool.h"
#include <cassert>


warthog::mem::csp_label_pool::csp_label_pool(size_t num_labels)
	: blocks_(0),head_free_(0),block_id_(0),label_id_(0)
{
    init(num_labels);
}


void
warthog::mem::csp_label_pool::init(size_t num_labels)
{
	num_blocks_ = ((num_labels) >> warthog::mem::csp_label_pool_ls::LOG2_LBS)+1;
	blocks_ = new warthog::csp_label* [num_blocks_]();
	for(size_t i=0; i < num_blocks_; i++)
	{
		blocks_[i] = 0;
	}

}

void
warthog::mem::csp_label_pool::resize(size_t new_block_size)
{
	warthog::csp_label** tmp = new warthog::csp_label* [new_block_size]();
    for(size_t i=0; i < num_blocks_; i++)
	{
		tmp[i] = blocks_[i];
    }
    
    delete [] blocks_;
    blocks_ = tmp;
    num_blocks_ = new_block_size;
}

warthog::mem::csp_label_pool::~csp_label_pool()
{
    for(size_t i=0; i < num_blocks_; i++)
	{
		if(blocks_[i] != 0)
		{
			delete [] blocks_[i];
            blocks_[i] = 0;
		}
	}
    delete [] blocks_;
}

warthog::csp_label* const
warthog::mem::csp_label_pool::get_label(void)
{
	warthog::csp_label* tmp =0;
    // id outside the pool address range 
	if(head_free_)
    { 
        tmp = head_free_;
        head_free_=tmp->next_;
        tmp->next_ = 0; 
        return tmp; 
    }
    else
    {
        if(!blocks_[block_id_])
        {
            blocks_[block_id_]= new warthog::csp_label[warthog::mem::csp_label_pool_ls::LBS]();
        }
        tmp = &blocks_[block_id_][label_id_];
        label_id_++;
        if (label_id_==warthog::mem::csp_label_pool_ls::LBS)
        {
            block_id_++;
            label_id_=0;
            if (block_id_==num_blocks_)
            {
                resize(num_blocks_*2);
            }
        }
    }
    return tmp;  
}

uint32_t
warthog::mem::csp_label_pool::pool_size(void)
{
	return (block_id_*warthog::mem::csp_label_pool_ls::LBS)+label_id_;    
}

void
warthog::mem::csp_label_pool::save_label(warthog::csp_label* label)
{
	label->next_=NULL;
    if(!head_free_)
    {
        head_free_=label;
    }
    else
    {
        label->next_=head_free_;
        head_free_=label;
    }
    return;    
}

size_t
warthog::mem::csp_label_pool::mem()
{
	size_t bytes = 
        sizeof(*this) + 
		num_blocks_*sizeof(void*);

	return bytes;
}
