#ifndef WARTHOG_CSP_LABEL_POOL_H
#define WARTHOG_CSP_LABEL_POOL_H

// memory/csp_label_pool.h
//
// A memory pool of warthog::cap_label.
//
// @author:
// @created:
//

#include "labels.h"
#include <iostream>
#include <stdint.h>

namespace warthog
{

namespace mem
{

namespace csp_label_pool_ls
{
	static const uint64_t LBS = 1024*32; // label block size; set this >= 8
	static const uint64_t LOG2_LBS = 15;
}

class csp_label_pool
{
	public:
        csp_label_pool(size_t num_labels);
		~csp_label_pool();

		warthog::csp_label* const
        get_label();

		void
        save_label(warthog::csp_label*);

		uint32_t
        pool_size(void);

		size_t
		mem();

	private:
        void init(size_t nblocks);
		void resize(size_t nblocks);

		size_t num_blocks_;
		warthog::csp_label** blocks_;
		warthog::csp_label* head_free_;
		uint16_t block_id_;
		uint16_t label_id_;

};

}

}

#endif

