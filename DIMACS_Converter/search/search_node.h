#ifndef WARTHOG_search_node_H
#define WARTHOG_search_node_H

// search_node.h
//
// @author: 
// @created: 
//

#include "constants.h"
#include "cpool.h"
#include "labels.h"

#include <iostream>

namespace warthog
{


class search_node
{
	public:
		search_node(warthog::sn_id_t id = warthog::SN_ID_MAX) : 
            id_(id),node_info_(0)
		{}

		~search_node()
		{}

		inline void
		init(warthog::cost_uint f, warthog::cost_uint g, warthog::cost_uint r, warthog::sn_id_t parent_id, bool index)
		{
			if (!node_info_)
            {
				master_label* init_info = new master_label;
            	node_info_ = init_info;
			}
            (node_info_->shortest_path_[index]) = {f , g, r, parent_id};
		}

        inline void 
		relax(warthog::cost_uint g, warthog::cost_uint r, warthog::sn_id_t parent_id, bool index)
		{
			assert(g < (node_info_->shortest_path_[index]).g_);
			(node_info_->shortest_path_[index]).f_ = ((node_info_->shortest_path_[index]).f_ - (node_info_->shortest_path_[index]).g_) + g;
			(node_info_->shortest_path_[index]).g_ = g;
			(node_info_->shortest_path_[index]).r_ = r;
			(node_info_->shortest_path_[index]).parent_id_ = parent_id;            
		}

		inline bool
		node_initialised() const 
		{return (node_info_!=0); }
        
        inline void
		clear_node() 
		{delete node_info_; node_info_ = 0; }

        inline warthog::sn_id_t
		get_id() const 
		{ return id_; }

		inline void
		set_id(warthog::sn_id_t id) 
		{id_ = id;}

		inline warthog::sn_id_t
		get_parent_id(bool index) const
		{return (node_info_->shortest_path_[index]).parent_id_;}

		inline warthog::csp_label*
		get_head(void) const
		{return node_info_->head_;}
		
		inline bool
		get_status(bool index) const 
		{return (node_info_->shortest_path_[index]).status_; }

		inline void
		set_status(bool index) 
		{ (node_info_->shortest_path_[index]).status_ = true; }

		inline void
		clear_status(bool index) 
		{ (node_info_->shortest_path_[index]).status_ = false; }

		inline uint32_t
		get_priority(bool index) const
		{ return (node_info_->shortest_path_[index]).priority_;}

		inline void
		set_priority(bool index, uint32_t pri)
		{ (node_info_->shortest_path_[index]).priority_ = pri;} 

		inline warthog::cost_uint 
		get_f(bool index) const 
		{ return (node_info_->shortest_path_[index]).f_;}

		inline warthog::cost_uint
		get_g(bool index) const
		{ return (node_info_->shortest_path_[index]).g_; }
		
		inline warthog::cost_uint 
		get_r(bool index) const 
		{ return (node_info_->shortest_path_[index]).r_;}

		inline warthog::cost_uint
		get_h(bool index) const
		{ return (node_info_->shortest_path_[index]).f_-(node_info_->shortest_path_[index]).g_;}

		inline warthog::cost_uint 
		get_r_min() const 
		{ return node_info_->r_min_;}
		
		inline void 
		set_r_min(warthog::cost_uint r_) 
		{ node_info_->r_min_ = r_;}

		// void 
		// add_link_list(csp_label* new_lb)  
		// {  
    	// 	new_lb->next_ = node_info_->head_;  
    	// 	node_info_->head_ = new_lb;
		// 	// list_size_++;
		// }

		// void 
		// add_expanded_link_list_rcbda(csp_label* new_lb)  
		// {  
    	// 	new_lb->next_ = node_info_->head_;  
    	// 	node_info_->head_ = new_lb;
		// }

		// bool 
		// add_expanded_link_list_pulse(csp_label* new_lb)  
		// {  
    	// 	if (node_info_->list_size_pulse_ > 1000) return false;
		// 	new_lb->next_ = node_info_->head_;  
    	// 	node_info_->head_ = new_lb;
		// 	node_info_->list_size_pulse_++;
		// 	return true;
		// }
		
		void 
		add_expanded_link_list_csp(csp_label* new_lb)  
		{  
    		// new_lb_->next_=0;
			if (!node_info_->head_)
			{   node_info_->head_=new_lb;
                node_info_->tail_=new_lb;
            }
			else
			{
                (node_info_->tail_)->next_ = new_lb;  
    		    node_info_->tail_ = new_lb;
			    // (node_info_->tail_)->next_ = NULL;
            }
		}
	
		// void 
		// add_expanded_link_list_csp(csp_label* new_lb)  
		// {  
    	// 	new_lb->next_ = node_info_->head_;  
    	// 	node_info_->head_ = new_lb;
		// }


		// inline bool
		// is_dominated_rcbda(warthog::cost_uint g, warthog::cost_uint r) 
		// { 	
		// 	csp_label* current = node_info_->head_;  
		// 	csp_label* prev = node_info_->head_;  
		// 	if (current == NULL){return false;}
		// 	while (current == node_info_->head_ && current != NULL)  
		// 	{ 
		// 		if(current->g_ <= g && current->r_ <= r) {return true;}
	    //  		if(current->g_ > g && current->r_ >= r)
		// 		{	current->status_ = false;
		// 			node_info_->head_ = current->next_;  
		// 			current = node_info_->head_;
		// 			prev = node_info_->head_;
		// 		}
		// 		else
		// 		{
		// 			current = (node_info_->head_)->next_;
		// 		}
		// 	}
		// 	while (current != NULL)  
		// 	{ 
		// 		if(current->g_ <= g && current->r_ <= r) {return true;}
     	// 		if(current->g_ > g && current->r_ >= r)
		// 		{	current->status_ = false;
		// 			prev->next_ = current->next_;  
		// 			current = current->next_;
		// 		}
		// 		else
		// 		{
		// 			prev = current;
		// 			current = prev->next_;
		// 		}
		// 	}
		// 	return false;
		// }

		// inline bool
		// is_dominated_pulse(warthog::cost_uint g, warthog::cost_uint r) 
		// { 	
			
		// 	for (uint i=0; i<3; i++)
		// 	{
		// 		if (g >= node_info_->non_dom_pulse_g_[i] && r >= node_info_->non_dom_pulse_r_[i])
		// 		{return true;}
		// 	}
			
		// 	if (g<node_info_->non_dom_pulse_g_[0])
		// 	{
		// 		node_info_->non_dom_pulse_g_[0] = g;
		// 		node_info_->non_dom_pulse_r_[0] = r;
		// 	}
			
		// 	else if (r<node_info_->non_dom_pulse_r_[1])
		// 	{
		// 		node_info_->non_dom_pulse_g_[1] = g;
		// 		node_info_->non_dom_pulse_r_[1] = r;
		// 	}
		// 	// random
		// 	else if ( (g+r)%2 )
		// 	{
		// 		node_info_->non_dom_pulse_g_[2] = g;
		// 		node_info_->non_dom_pulse_r_[2] = r;
		// 	}

		// 	return false;
		// }
		
		
		// inline uint
		// get_join_ref_pulse(warthog::cost_uint g, warthog::cost_uint r, warthog::cost_uint upper_bounds[2]) const
		// {
		// 	uint index = 0;
		// 	for (uint i=0; i<3; i++)
		// 	{
		// 		if (node_info_->non_dom_pulse_g_[i]!= warthog::COST_UINT_MAX)
		// 			{if (g + node_info_->non_dom_pulse_g_[i] < upper_bounds[0] && r + node_info_->non_dom_pulse_r_[i]  <= upper_bounds[1])
		// 			index = i+1;}
		// 	}
		// 	return index;
		// }

		// inline warthog::cost_uint
		// get_g_pulse(uint8_t index) const
		// {
		// 	return node_info_->non_dom_pulse_g_[index];
		// }

		// inline warthog::cost_uint
		// get_r_pulse(uint8_t index) const
		// {
		// 	return node_info_->non_dom_pulse_r_[index];
		// }

		// inline void
		// set_active_pulse(bool status)
		// {
		// 	node_info_->active_pulse_ = status;
		// }

		// inline bool
		// get_active_pulse(void) const
		// {
		// 	return node_info_->active_pulse_;
		// }

		// inline csp_label*
		// get_join_index_rcbda(csp_label* rev_label, warthog::cost_uint upper_bounds[2]) const
		// {
		// 	warthog::cost_uint g_ub = warthog::COST_UINT_MAX;
		// 	csp_label* join_ref = NULL;
		// 	csp_label* current = node_info_->head_;  
		// 	csp_label* tmp = NULL;  
		// 	while (current != NULL)  
		// 	{ 
		// 		if(current->g_ + rev_label->g_ < upper_bounds[0] && current->r_ + rev_label->r_ <= upper_bounds[1])
		// 		{
		// 			g_ub = current->g_ + rev_label->g_;
		// 			join_ref = current;
		// 		}
		// 		tmp = current->next_;
		// 		current = tmp;
		// 	}
		// 	return join_ref;
		// }

		
		// inline csp_label*
		// get_join_index_csp(csp_label* rev_label, warthog::cost_uint upper_bounds[2]) const
		// {
		// 	// warthog::cost_uint g_ub = warthog::COST_UINT_MAX;
		// 	csp_label* result = NULL;
		// 	csp_label* current = node_info_->head_;  
		// 	csp_label* tmp = NULL;  
			
		// 	while (current != NULL )  
		// 	{ 
				
		// 		if(current->r_ + rev_label->r_ <= upper_bounds[1])
		// 		{
		// 			result = current;
		// 			tmp = current->next_;
		// 			current = tmp;
		// 		}
		// 		else
		// 		{
		// 			break;
		// 		}
								
		// 	}
		// 	return result;
		// }

		inline csp_label*
		get_join_index_csp(csp_label* rev_label, warthog::cost_uint upper_bounds[2]) const
		{
			// warthog::cost_uint g_ub = warthog::COST_UINT_MAX;
			csp_label* result = NULL;
			csp_label* current = node_info_->head_;  
			csp_label* tmp = NULL;  
			while (current != NULL)  
			{ 
				if(current->r_ + rev_label->r_ <= upper_bounds[1] && rev_label->g_+current->g_ <= upper_bounds[0])
				{
					result = current;
					break;
				}
				else if (rev_label->g_+current->g_ > upper_bounds[0])
				{
					break;
				}
				else
				{
					tmp = current->next_;
					current = tmp;
				}
								
			}
			return result;
		}

		

		uint32_t
		mem()
		{
			return sizeof(*this);
		}

        // static uint32_t 
        // get_refcount()
		// { return refcount_; }

		
	private:
	
		warthog::sn_id_t id_;
        warthog::master_label* node_info_;

};


}

#endif

